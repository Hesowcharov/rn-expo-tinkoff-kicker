import { Match, Stage, Team, Tournament, TournamentStatus } from '../models/types'

export const UNIQUE_TEAM_NAMES = [
  "The B-Team",
  "The Underachievers",
  "Baby Boomers",
  "College Dropouts",
  "Low Expectations",
  "Bottom of the Barrel",
  "The Weakest Links",
  "Inferiority Complex",
  "Something Inoffensive",
  "Epic Failures",
  "Village Idiots",
  "Second Place",
  "The Expansion Pack",
  "Civil Disobedience",
  "Fire Extinguishers",
  "Gold Diggers",
  "One-Hit Wonders",
  "The Oversleepers",
  "Troublemakers",
  "Prodigies",
  "Virtuosos",
  "The Heat Wave",
  "Collision Course",
  "Exterminators",
  "Fire Starters",
  "Kryptonite",
  "No Mercy",
  "Power House",
  "Trailblazers",
  "The Shakedown",
  "Wolf Pack",
  "Lethal Weapons",
  "Outlaws",
  "Horsemen of the Apocalypse",
]

const shuffleArray = <P, T extends Array<P>> (array: T) => {
  return array.sort(() => Math.random() - 0.5)
}

export const generateTeams = (number: number): Team[] => {
  const shuffleNames = shuffleArray(UNIQUE_TEAM_NAMES)
  const randomTeams: Team[] = Array.from({length: number}, (_, idx) => {
    return {
      id: idx,
      name: shuffleNames[idx]
    }
  })
  return randomTeams
}

/**
 * generates stages for teams
 * @param teams a number of powers of two!
 * @param completed whether stages have been played
 * @returns
 */
export const generateStages = (teams: Team[], completed: boolean): Stage[] => {
  let stages: Stage[] = []

  let stageTeams = teams
  const levels = Math.log2(teams.length)
  let level = 1
  let lastMatchId = 1

  while (level <= levels) {
    const stageMatches: Match[] = []
    const stageTeamWinners: Team[] = []

    for (let idx = 0; idx < stageTeams.length; idx = idx + 2) {
      const team1 = teams[idx]
      const team2 = teams[idx+1]
      let score1 = 0
      let score2 = 0

      if (completed) {
        const isTeam1Winner = Math.random() > 0.5
        score1 = isTeam1Winner ? 10 : Math.floor(Math.random() * 10)
        score2 = isTeam1Winner ? Math.floor(Math.random() * 10) : 10
        stageTeamWinners.push(isTeam1Winner ? team1 : team2)
      }

      const match: Match = {
        id: lastMatchId,
        team1,
        team2,
        score1,
        score2,
      }

      stageMatches.push(match)
      lastMatchId++
    }
    stages.push({
      level,
      matches: stageMatches
    })
    level++
    stageTeams = shuffleArray(stageTeamWinners)
  }
  return stages
}

/**
 * generates a match with 2 random teams
 */
export const generateMatch = (completed: boolean): Match => {
  const [team1, team2] = generateTeams(2)

  let score1 = 0
  let score2 = 0
  if (completed) {
    const isTeam1Winner = Math.random() > 0.5
    score1 = isTeam1Winner ? 10 : Math.floor(Math.random() * 10)
    score2 = isTeam1Winner ? Math.floor(Math.random() * 10) : 10
  }

  return {
    id: Math.floor(Math.random() *  10_000),
    team1,
    team2,
    score1,
    score2,
  }
}

/**
 * generate a tournament with completed stages
 * @param teamNumber a number of powers of two!
 */
export const generateCompletedTournament = (teamNumber: number): Tournament => {
  const teams = generateTeams(teamNumber)

  return {
    id: 0,
    name: "Fake tournament",
    status: TournamentStatus.FINISHED,
    stages: generateStages(teams, true)
  }
}

/**
 * generate an unplayed tournament with blank stages
 * @param teamNumber a number of powers of two!
 */
export const generateBlankTournament = (teamNumber: number): Tournament => {
  const teams = generateTeams(teamNumber)

  return {
    id: 0,
    name: "Fake tournament",
    status: TournamentStatus.ACTIVE,
    stages: generateStages(teams, false)
  }
}

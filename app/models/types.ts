export interface Team {
  name: string;
  id: number;
}

export interface Match {
  id: number;
  team1: Team;
  team2: Team;
  score1: number;
  score2: number;
}

export interface Stage {
  level: number;
  matches: Match[];
}

export enum TournamentStatus {
  OPEN = "OPEN",
  ACTIVE = "ACTIVE",
  FINISHED = "FINISHED",
}

export interface Tournament {
  id: number;
  name: string;
  status: TournamentStatus;
  stages: Stage[];
}
